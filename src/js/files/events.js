
import { addMoney, deleteMoney, noMoney, addRemoveClass } from './functions.js';
import { } from './script.js';
import { startData } from './startData.js';


// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {

	const wrapper = document.querySelector('.wrapper');

	const targetElement = e.target;

	const money = +localStorage.getItem('money');
	const bet = +localStorage.getItem('current-bet');

	// privacy screen
	if (targetElement.closest('.preloader__button')) {
		location.href = 'main.html';
	}

	// main screen

	if (targetElement.closest('[data-button="privacy"]')) {
		location.href = 'index.html';
	}

	if (targetElement.closest('[data-button="news-screen"]')) {
		wrapper.classList.add('_news-screen');
	}
	if (targetElement.closest('[data-button="news-screen-home"]')) {
		wrapper.classList.remove('_news-screen');
	}

	if (targetElement.closest('[data-button="news-1"]')) {
		wrapper.classList.remove('_news-screen');
		wrapper.classList.add('_news-1');
	}
	if (targetElement.closest('[data-button="news-1-home"]')) {
		wrapper.classList.remove('_news-1');
		wrapper.classList.add('_news-screen');
	}

	if (targetElement.closest('[data-button="news-2"]')) {
		wrapper.classList.remove('_news-screen');
		wrapper.classList.add('_news-2');
	}
	if (targetElement.closest('[data-button="news-2-home"]')) {
		wrapper.classList.remove('_news-2');
		wrapper.classList.add('_news-screen');
	}

	if (targetElement.closest('[data-button="news-3"]')) {
		wrapper.classList.remove('_news-screen');
		wrapper.classList.add('_news-3');
	}
	if (targetElement.closest('[data-button="news-3-home"]')) {
		wrapper.classList.remove('_news-3');
		wrapper.classList.add('_news-screen');
	}

	if (targetElement.closest('[data-button="news-4"]')) {
		wrapper.classList.remove('_news-screen');
		wrapper.classList.add('_news-4');
	}
	if (targetElement.closest('[data-button="news-4-home"]')) {
		wrapper.classList.remove('_news-4');
		wrapper.classList.add('_news-screen');
	}

	if (targetElement.closest('[data-button="news-5"]')) {
		wrapper.classList.remove('_news-screen');
		wrapper.classList.add('_news-5');
	}
	if (targetElement.closest('[data-button="news-5-home"]')) {
		wrapper.classList.remove('_news-5');
		wrapper.classList.add('_news-screen');
	}

	if (targetElement.closest('[data-button="news-6"]')) {
		wrapper.classList.remove('_news-screen');
		wrapper.classList.add('_news-6');
	}
	if (targetElement.closest('[data-button="news-6-home"]')) {
		wrapper.classList.remove('_news-6');
		wrapper.classList.add('_news-screen');
	}

})





